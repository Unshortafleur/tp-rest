package com.spring.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.spring.demo.domain.Product;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long>{

    Product save(Product entity);
    List<Product> findByName(String name);
}
