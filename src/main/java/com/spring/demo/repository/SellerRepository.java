package com.spring.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.spring.demo.domain.Seller;

import java.util.List;


public interface SellerRepository extends CrudRepository<Seller, Long>{


    List<Seller> findByName(String name);
    List<Seller> findAll();
    Long save();


}
