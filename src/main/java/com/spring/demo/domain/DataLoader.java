package com.spring.demo.domain;

import com.spring.demo.repository.SellerRepository;
import com.spring.demo.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class DataLoader {

    private SellerRepository selRepo;
    private ProductRepository prodRepo;

    @Autowired
    public DataLoader(SellerRepository selRepo, ProductRepository prodRepo) {
        this.selRepo = selRepo;
        this.prodRepo = prodRepo;
        loadData();
    }

    @PostConstruct
    private void loadData()
    {
        Seller sel = new Seller();
        sel.setName("Ventura");
        sel.setId(null);
        sel.setAdress("Toulouse");
        sel.setZipCode("31200");

        Product pro = new Product();
        pro.setDescription("description");
        pro.setId(null);
        pro.setName("Product1");
        double price = 200;
        pro.setPrice(price);
        pro.setSeller(sel);

        selRepo.save(sel);
        prodRepo.save(pro);

    }


}
