package com.spring.demo.service;

import com.spring.demo.domain.Product;
import com.spring.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private ProductRepository proRepo;

    @Autowired
    public ProductService(ProductRepository proRepo) {
        super();
        this.proRepo = proRepo;
    }

    public Product findByName(String name) {
        List<Product> products = proRepo.findByName(name);
        return (products.isEmpty()) ? null : products.get(0);
    }
}
