package com.spring.demo.service;
import com.spring.demo.domain.Seller;
import com.spring.demo.repository.SellerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class SellerService {

    private SellerRepository selRepo;

    @Autowired
    public SellerService(SellerRepository selRepo) {
        super();
        this.selRepo = selRepo;
    }

    public Seller findByName(String name) {
        List<Seller> sellers = selRepo.findByName(name);
        return (sellers.isEmpty()) ? null : sellers.get(0);
    }

    public Seller findAll() {
        List<Seller> sellers = selRepo.findAll();
        return (sellers.isEmpty()) ? null : sellers.get(0);
    }

    public Long save( Seller seller)
    {
        Long idSeller = selRepo.save(seller).getId();
        return idSeller;
    }

}
