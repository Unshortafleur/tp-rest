package com.spring.demo.Controller;

import com.spring.demo.domain.Product;
import com.spring.demo.domain.Seller;
import com.spring.demo.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.spring.demo.service.ProductService;
import com.spring.demo.service.SellerService;


@RestController
@RequestMapping("/sellers")
public class SellerController {

    private SellerService sellerserv;

    public SellerController(SellerService sellerserv){
        super();
        this.sellerserv = sellerserv;
    }

    @GetMapping("/{name}")
    public ResponseEntity<Seller> getProductByName(@PathVariable String name)
    {
        return new ResponseEntity<Seller>(sellerserv.findByName(name),HttpStatus.OK);

    }
    @GetMapping("/")
    public ResponseEntity<Seller> getAllProducts(@PathVariable String name)
    {
        return new ResponseEntity<Seller>(sellerserv.findAll(),HttpStatus.OK);
    }

    @PostMapping("/create")
    public Long createSeller(@RequestBody Seller seller)
    {
        return sellerserv.save(seller);
    }

}
