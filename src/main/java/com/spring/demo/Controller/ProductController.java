package com.spring.demo.Controller;
import com.spring.demo.domain.Product;
import com.spring.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/products")
public class ProductController {

    private ProductService Productserv;

    @Autowired
    public ProductController(ProductService Productserv){
        super();
        this.Productserv = Productserv;
    }

    @GetMapping("/{name}")
    public ResponseEntity<Product> getProductByName(@PathVariable String name)
    {
        return new ResponseEntity<Product>(Productserv.findByName(name),HttpStatus.OK);

    }
}